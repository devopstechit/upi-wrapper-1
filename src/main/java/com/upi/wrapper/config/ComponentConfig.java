package com.upi.wrapper.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.m2p.root.exception.ApiExceptionBuilder;
import com.m2p.root.integrator.remote.service.RemoteService;
import com.m2p.root.integrator.remote.service.impl.RemoteServiceImpl;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ComponentConfig {

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource
                = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(
                "classpath:messages",
                "classpath:m2p_messages",
                "classpath:upi_messages"
        );
        return messageSource;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public RemoteService remoteService() {
        return new RemoteServiceImpl();
    }

    @Bean
    ApiExceptionBuilder exceptionBuilder() {
        return new ApiExceptionBuilder("com.upi.wrapper");
    }

}
