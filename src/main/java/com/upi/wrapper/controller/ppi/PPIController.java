package com.upi.wrapper.controller.ppi;


import com.m2p.root.exception.ApiException;
import com.m2p.root.exception.ApiExceptionBuilder;
import com.upi.common.dto.request.profile.RegisterPPIProfileDto;
import com.upi.common.enums.ApiResponseStatus;
import com.upi.common.utility.UpiResponse;
import com.upi.wrapper.service.impl.PPIServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(value = "upi/v1/wrapper/ppi/")
public class PPIController {
    @Autowired
    ApiExceptionBuilder exceptionBuilder;

    @Autowired
    PPIServiceImpl ppiServiceImpl;

    @PostMapping(value = "registerProfile", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UpiResponse<String>> registerPPIProfile(@Valid @RequestBody RegisterPPIProfileDto request, Errors errors) {
        try {
            log.debug("Request received for checkIfVpaAvailable: " + request);
            ApiException.checkValidationErrors(exceptionBuilder, errors);
            return new ResponseEntity<>(ppiServiceImpl.registerPPIProfile(request), HttpStatus.OK);
        } catch (ApiException e) {
            log.error("Exception in checkIfVpaAvailable: " + e.getDetailedMessage());
            return new ResponseEntity<>(UpiResponse.<String>builder().status(ApiResponseStatus.FAILURE).exception(e).seqNo(request.getSeqNo()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
