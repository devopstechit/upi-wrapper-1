package com.upi.wrapper.contants.common;

public class MessageConstants {

    public static final String RESPONSE_FROM_UPI_PROFILE = "Response from UPI Profile service";

    private MessageConstants() {
    }
}
